package br.edu.up.modelos;

public class ClienteEmpresa extends Cliente {

    private String cnpj;
    private String inscricaoEstadual;
    private int anoFundacao;

    public ClienteEmpresa(String nome, String telefone, String email, String cnpj, String inscricaoEstadual,
            int anoFundacao) {
        super(nome, telefone, email);
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.anoFundacao = anoFundacao;
    }

    

    public String getCnpj() {
        return cnpj;
    }



    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }



    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }



    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }



    public int getAnoFundacao() {
        return anoFundacao;
    }



    public void setAnoFundacao(int anoFundacao) {
        this.anoFundacao = anoFundacao;
    }


    public double getCredito() {
        return vlrMaximoCredito - vlrEmprestado;
    }

    public void adicionarEmprestimo(double vlr) {
        this.vlrEmprestado += vlr;
    }

    public double getFatura(){
        return vlrMaximoCredito - vlrEmprestado;
    }

    public double pagarEmprestimo(Double pago){
        return vlrEmprestado -= pago;
    }

    @Override
    public String toString() {
        return "ClienteEmpresa [nome=" + nome + ", cnpj=" + cnpj + ", telefone=" + telefone + ", inscricaoEstadual="
                + inscricaoEstadual + ", email=" + email + ", anoFundacao=" + anoFundacao + ", emprestado=" + vlrEmprestado + "]";
    }

    @Override
    public String toCSV() {
        return nome + ";" + telefone + ";" + email + ";;;;" + cnpj + ";" + inscricaoEstadual + ";" + anoFundacao;
    }

}
